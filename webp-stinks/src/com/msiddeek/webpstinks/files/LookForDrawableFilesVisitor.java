package com.msiddeek.webpstinks.files;

import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.vfs.VfsUtilCore;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileVisitor;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class LookForDrawableFilesVisitor extends VirtualFileVisitor<Object> {
    private static final Logger LOG = Logger.getInstance(LookForDrawableFilesVisitor.class);

    private final Set<VirtualFile> mWebps;
    private final Set<VirtualFile> mXmls;
    private final Pattern mPattern;

    public LookForDrawableFilesVisitor(List<String> drawables) {
        mWebps = new HashSet<VirtualFile>();
        mXmls = new HashSet<VirtualFile>();
        mPattern = createDrawablesRegex(drawables);
    }

    @Override
    public boolean visitFile(@NotNull VirtualFile file) {
        String path = file.getCanonicalPath();
        if (path == null) {
            return false;
        }
        if (mPattern.matcher(file.getCanonicalPath()).find()) {
            if (path.endsWith(".webp")) {
                mWebps.add(file);
            } else if (path.endsWith(".xml")) {
                mXmls.add(file);
            }
        }
        return !file.getCanonicalPath().contains(".");
    }

    public Set<VirtualFile> getWebps() {
        return mWebps;
    }

    public Set<VirtualFile> getXmls() {
        return mXmls;
    }

    private static Pattern createDrawablesRegex(List<String> webpNames) {
        StringBuilder regex = new StringBuilder("^.+/drawable.*/(");
        if (!webpNames.isEmpty()) {
            regex.append("(").append(webpNames.get(0)).append(")");
            for (int i = 1; i < webpNames.size(); ++i) {
                regex.append("|(").append(webpNames.get(i)).append(")");
            }
        }
        regex.append(")\\..+$");
        return Pattern.compile(regex.toString());
    }
}