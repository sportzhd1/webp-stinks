package com.msiddeek.webpstinks.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.actionSystem.ex.AnActionListener;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.module.ModuleUtilCore;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.msiddeek.webpstinks.utils.AndroidDirUtils;
import com.msiddeek.webpstinks.utils.AndroidResUtils;

import java.util.HashSet;
import java.util.Set;

import static com.intellij.openapi.actionSystem.PlatformDataKeys.EDITOR;
import static com.intellij.openapi.actionSystem.PlatformDataKeys.VIRTUAL_FILE;

public class EditActionListener implements AnActionListener {
    private static final Logger LOG = Logger.getInstance(EditActionListener.class);

    private static final String OUTPUT_FORMAT = "png";
    private static final int MAX_RETRIES = 5;
    private static final int CLEANUP_DELAY = 5000; // 5 secs
    private static final int MAX_SEARCH_ITERATIONS = 5;

    private final Set<VirtualFile> mOuts;

    public EditActionListener() {
        mOuts = new HashSet<VirtualFile>();
    }

    private boolean shouldListen(AnAction action) {
        if (action == null) {
            return false;
        }
        String actionName = action.getClass().getName().toLowerCase();
        return actionName.contains("refresh");
    }

    private Document getDocument(AnActionEvent event) {
        Editor editor = event.getData(EDITOR);
        if (editor != null) {
            return editor.getDocument();
        }
        VirtualFile file = event.getData(VIRTUAL_FILE);
        if (file != null && file.getCanonicalPath() != null && file.getCanonicalPath().contains("res")) {
            return FileDocumentManager.getInstance().getDocument(file);
        }
        return null;
    }

    @Override
    public void beforeEditorTyping(char c, DataContext dataContext) {
        // No-op
    }

    private VirtualFile getResRoot(Project project, VirtualFile resFile) {
        if (resFile == null) {
            return project.getBaseDir();
        }
        if (resFile.getCanonicalPath() == null
                || !resFile.getCanonicalPath().contains("res")) {
            return ModuleUtilCore.findModuleForFile(resFile, project).getModuleFile().getParent();
        }
        while (resFile.getParent() != null && !resFile.getName().endsWith("res")) {
            resFile = resFile.getParent();
        }
        return resFile;
    }

    @Override
    public synchronized void beforeActionPerformed(AnAction action, DataContext dataContext, AnActionEvent event) {
        if (mOuts.isEmpty() && shouldListen(action)) {
            mOuts.add(null);
            LOG.info("listened: " + action);
            VirtualFile root = getResRoot(event.getProject(), event.getData(VIRTUAL_FILE));
            Set<VirtualFile> ins = AndroidDirUtils.getWebpsInDocument(root, MAX_SEARCH_ITERATIONS);
            LOG.info("converting:\n" + ins);
            mOuts.addAll(AndroidResUtils.convert(ins, OUTPUT_FORMAT, MAX_RETRIES));
            LOG.info("got:\n" + mOuts);
        } else {
            LOG.info("ignored: " + action);
        }
    }

    @Override
    public synchronized void afterActionPerformed(AnAction action, DataContext dataContext, AnActionEvent event) {
        if (shouldListen(action)) {
            while (mOuts.contains(null)) {
                mOuts.remove(null);
            }
            LOG.info("cleanup:\n" + mOuts);
            AndroidResUtils.delete(mOuts, MAX_RETRIES, CLEANUP_DELAY);
        }
    }
}