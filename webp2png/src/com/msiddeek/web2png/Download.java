package com.msiddeek.web2png;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Download {
	private static boolean isRedirected(HttpURLConnection header) {
		for (int i = 0; header.getHeaderField(i) != null; ++i) {
            String hv = header.getHeaderField(i);
			if (hv.indexOf("301") != -1 || hv.indexOf("302") != -1)
				return true;
		}
		return false;
	}

	public static File download(String path, String name, File to)
			throws IOException {

		System.out.println("download...");
		URL url = new URL(path + name);
		HttpURLConnection http = (HttpURLConnection) url.openConnection();
		while (isRedirected(http)) {
			url = new URL(http.getHeaderField("Location"));
			http = (HttpURLConnection) url.openConnection();
		}
		InputStream input = http.getInputStream();
		byte[] buffer = new byte[4096];
		int n = -1;
		to.mkdirs();
		OutputStream output = new FileOutputStream(new File(to, name));
		while ((n = input.read(buffer)) != -1) {
			output.write(buffer, 0, n);
		}
		output.close();
		System.out.println(to + " downloaded.");
		return to;
	}
}