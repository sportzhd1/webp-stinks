package com.msiddeek.web2png;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Copy {


    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[1024];
        while (in.read(b) != -1) {
            out.write(b);
        }
    }

	public static File copy(File from, File to)
			throws FileNotFoundException, IOException {
		copy(new FileInputStream(from), new FileOutputStream(to));
		System.out.println(to + " copied.");
        return to;
    }

}
