package com.msiddeek.webpstinks.component;

import com.intellij.openapi.actionSystem.ex.ActionManagerEx;
import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.msiddeek.web2png.WebP;
import com.msiddeek.webpstinks.action.EditActionListener;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public class Webp implements ProjectComponent {
    private static final Logger LOG = Logger.getInstance(Webp.class);

    private final Project mProject;
    private final EditActionListener mEditActionListener;

    public Webp(Project project) {
        mProject = project;
        mEditActionListener = new EditActionListener();
    }

    @Override
    public void initComponent() {
        try {
            File root = new File(mProject.getWorkspaceFile().getParent().getCanonicalPath(), "/.web2png/");
            if (!WebP.isInitialized(root)) {
                WebP.init(root);
            }
            ActionManagerEx.getInstanceEx().addAnActionListener(mEditActionListener);
            LOG.info("Initialized WebpStinks");
        } catch (Exception e) {
            LOG.error(e); ;
        }
    }

    public void disposeComponent() {
        ActionManagerEx.getInstanceEx().removeAnActionListener(mEditActionListener);
    }

    @NotNull
    public String getComponentName() {
        return "Webp";
    }

    public void projectOpened() {
        // No-op
    }

    public void projectClosed() {
        // No-op
    }
}
