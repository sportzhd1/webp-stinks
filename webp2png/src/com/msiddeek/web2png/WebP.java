package com.msiddeek.web2png;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;

import com.luciad.imageio.webp.WebPImageReaderSpi;

public class WebP {

	private static class WebpBinary {
		private static final WebpBinary MAC = new WebpBinary("macosx-universal", "libwebp-imageio.dylib");
        private static final WebpBinary WINDOWS = new WebpBinary("windows-x86", "webp-imageio.dll");
        private static final WebpBinary LINUX = new WebpBinary("linux-x86", "libwebp-imageio.so");
        private static final WebpBinary[] VALUES = new WebpBinary[] { MAC, WINDOWS, LINUX };

		private final String mPath;
		private final String mName;
		
		private WebpBinary(String path, String name) {
			mPath = path;
			mName = name;
		}
    }
	
	public static boolean isInitialized(File root) throws Exception {
    	for (int i = 0; i <  WebpBinary.VALUES.length; ++i) {
    		if (!new File(root, WebpBinary.VALUES[i].mName).exists())
    			return false;
    	}
        JavaLibraryPath.append(root.getAbsolutePath());
    	return true;
	}

    public static void init(File root) throws Exception {
		System.out.println("init...");
		Download.download(
				"https://bitbucket.org/luciad/webp-imageio/downloads/",
				"webp-imageio-0.4.2.zip",
				root);
		Zip.unzip(new File(root, "webp-imageio-0.4.2.zip"), root);
        for (int i = 0; i <  WebpBinary.VALUES.length; ++i) {
			Copy.copy(
					new File(root, "webp-imageio-0.4.2/" + WebpBinary.VALUES[i].mPath + "/" + WebpBinary.VALUES[i].mName),
					new File(root, WebpBinary.VALUES[i].mName));
    	}
        JavaLibraryPath.append(root.getAbsolutePath());
    }


	public static BufferedImage read(InputStream imageInputStream)
			throws IOException {

		ImageReader reader = new WebPImageReaderSpi().createReaderInstance("webp");
        reader.setInput(ImageIO.createImageInputStream(imageInputStream));
        return reader.read(0);
	}

	public static void convert(File root, String webp) throws IOException {
		System.out.println("convert...");
        BufferedImage image = read(new FileInputStream(new File(root, webp)));

		File output = new File(root, webp.substring(0, webp.length() - 4) + "png");
        FileOutputStream outputBuffer = new FileOutputStream(output);
        ImageIO.write(image, "png", outputBuffer);

		System.out.println(output + " converted.");
	}
	
	public static void main(String[] args) throws Exception {
		File root = new File("out/webp2png");
		if (!isInitialized(root)) {
            init(root);
        }
		if (isInitialized(root)) {
			convert(root, args[0]);
		}
	}

}
